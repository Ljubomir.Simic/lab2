package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    List<FridgeItem> items = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            items.add(item);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        List<FridgeItem> toRemove = new ArrayList<>();
        if (items.contains(item)) {
            for (FridgeItem i: items) {
                if (i.getName().equals(item.getName())) {
                    toRemove.add(item);
                }
            }
            items.removeAll(toRemove);
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<>();
        for (FridgeItem item: items) {
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }
        items.removeAll(expiredItems);
        return expiredItems;
    }
}
